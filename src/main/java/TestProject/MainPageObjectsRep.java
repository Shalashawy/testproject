package TestProject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPageObjectsRep {

	private WebDriver driver;
	private WebDriverWait wait;

	public MainPageObjectsRep(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 10);
		PageFactory.initElements(driver, this);
	}
	
	//web Elements
	@FindBy(linkText = "Free Sign Up")
	public WebElement signUpButton;
	
	
	
	//Actions
	public void clickSignUp()
	{
		WebDriverWait wait= new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(signUpButton));
		signUpButton.click();
	}
	
	
}
