package TestProject;

import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MainPageTest extends Base{
	
	@BeforeTest
	public void envSetup() throws IOException, InterruptedException {
		setup();
		
			}
	@Test
	public void signUp() throws InterruptedException
	{
		//Thread.sleep(2000);
		MainPageObjectsRep Rep=new MainPageObjectsRep(driver);
		Rep.clickSignUp();
		
	}

	@AfterTest
	public void closeDriver()
	{
		driver.close();
	}
}
